let Scores = [
    {
        studentName: "Phan Van Teo",
        math: 8.7,
        physical: 7.2,
        chemistry: 6.9,
        literature: 7.8,
        history: 7.6,
        geography: 9.1
    },
    {
        studentName: "Ban Thi No",
        math: 9.2,
        physical: 6.5,
        chemistry: 7.0,
        literature: 6.3,
        history: 5.6,
        geography: 8.5
    },
    {
        studentName: "Ton That Hoc",
        math: 10,
        physical: 10,
        chemistry: 10,
        literature: 10,
        history: 9.5,
        geography: 9.8
    },

]
function diemtb(tb){
    let arr=Object.keys(tb)//lay key studentName,math,physical,chemistry,literature,history,geography
    let sum=0;
    for(let i=0;i<arr.length;i++)
    {
        if(typeof tb[arr[i]]==='number'){//kieu du lieu trong mang 
        sum+=tb[arr[i]];
    }
    }
    return sum/arr.length;
}
let max=0
let index=0;// tim vi tri
for(let i=0;i<Scores.length;i++)
{
    if(max < diemtb(Scores[i]))
    {
        max = diemtb(Scores[i]);
        index=i//gan 

    }
}
console.log("hoc sinh co diem trung binh cao nhat la:"+Scores[index].studentName);